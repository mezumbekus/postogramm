<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<body class="home page">

<div class="wrapper">
    <header>
        <div class="header-top">
            <div class="container">
                <div class="col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4 brand-logo">
                    <h1>
                        <a href="#">
                            <img src="img/logo.png" alt="">
                        </a>
                    </h1>
                </div>
                <div class="col-md-4 col-sm-4 navicons-topbar">
                    <ul>
                        <li class="blog-search">
                            <a href="#" title="Search"><i class="fa fa-search"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="header-main-nav">
            <div class="container">
                <div class="main-nav-wrapper">
                    <nav class="main-menu">
                        <?php
                        $menuItems = [
                            ['label' => 'Home', 'url' => ['/site/index']],
                        ];
                        if (Yii::$app->user->isGuest) {
                            $menuItems[] = ['label' => 'Signup', 'url' => ['/user/default/signup']];
                            $menuItems[] = ['label' => 'Login', 'url' => ['/user/default/login']];
                        } else {
                            $menuItems[] = '<li>'
                                . Html::beginForm(['/user/default/logout'], 'post')
                                . Html::submitButton(
                                    'Logout (' . Yii::$app->user->identity->firstname . ')',
                                    ['class' => 'btn btn-link logout']
                                )
                                . Html::endForm()
                                . '</li>';
                            $menuItems[] = ['label' => 'Generate Users', 'url' => ['/user/profile/generate']];
                            $menuItems[] = ['label' => 'Edit profile', 'url' => ['/edit']];
                            $menuItems[] = ['label' => 'Создать пост', 'url' => ['/post/create']];
                        }
                        echo Nav::widget([
                            'options' => ['class' => 'navbar-nav navbar-right menu'],
                            'items' => $menuItems,
                        ]);
                        ?>
                    </nav>
                </div>
            </div>
        </div>

    </header>


    <div class="container full">

        <div class="page-posts no-padding">
            <div class="row">
                <div class="page page-post col-sm-12 col-xs-12">
                    <div class="blog-posts blog-posts-large">

                        <div class="row">

                            <?= Alert::widget() ?>
                            <?= $content ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



</body>

<footer>
    <div class="footer">
        <div class="back-to-top-page">
            <a class="back-to-top"><i class="fa fa-angle-double-up"></i></a>
        </div>
        <p class="text">Images | 2017</p>
    </div>
</footer>


<?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>
