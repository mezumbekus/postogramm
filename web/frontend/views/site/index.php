<?php
use yii\helpers\Url;
//var_dump($feedUsers);die;
?>
<?php foreach ($feedUsers as $feed): ?>
<article class="post col-sm-12 col-xs-12">
    <div class="post-meta">
        <div class="post-title">
            <img src="<?= $feed->author_picture ?>" class="author-image" />
            <div class="author-name"><a href="<?= Url::to(['/profile/']).'/'.$feed->author_id ?>"><?= $feed->author_name ?></a></div>
        </div>
    </div>
    <div class="post-type-image">
        <a href="#">
            <img src="<?= Yii::$app->params['storageUri'].$feed->post_filename ?>" alt="">
        </a>
    </div>
    <div class="post-description">
        <p><?= $feed->post_description ?></p>
    </div>
    <div class="post-bottom">
        <div class="post-likes">
            <a href="#" class="btn btn-secondary"><i class="fa fa-lg fa-heart-o"></i></a>
            <span><?= $feed->countLikes() ?> Likes</span>
        </div>
        <div class="post-comments">
            <a href="#">6 Comments</a>

        </div>
        <div class="post-date">
            <span><?= $feed->post_created_at ?></span>
        </div>
        <?php if($feed->isReported($user)): ?>
        <div class="post-report">
            <a href="" data-id = "<?= $feed->post_id ?>">Report post</a>
        </div>
        <?php else: ?>
            <div class="post-report">
                Reported!
            </div>
        <?php endif; ?>
    </div>
</article>

<?php endforeach; ?>
