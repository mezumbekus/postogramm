<?php

namespace frontend\models;

use yii\base\Model;


class SignupForm extends Model
{
    public $firstname;
    public $lastname;
    public $email;
    public $phonenumber;
    public $username;
    public $password;

    public function rules()
    {
        return [
            [['username','firstname','lastname','email','phonenumber','password'],'required']
        ];
    }

    public function attributeLabels()
    {
        return [
            'firstname' => 'Имя',
            'lastname' => 'Фамилия',
            'email' => 'Электронный адрес',
            'phonenumber' => 'Телефонный номер',
            'username' => 'Никнейм',
        ];
    }

}