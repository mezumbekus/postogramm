<?php

namespace app\models;

use Yii;
use frontend\models\User;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property int $user_id
 * @property string $filename
 * @property string $description
 * @property int $created_at
 */
class Post extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'post';
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'filename' => 'Filename',
            'description' => 'Description',
            'created_at' => 'Created At',
        ];
    }

    public function sendReport(User $user)
    {
        $redis = Yii::$app->redis;
        $k = "post:{$this->getId()}:reports";
        if(!$redis->sismember($k,$user->getId())) {
            $redis->sadd($k,$user->getId());
            return true;
        }
        return false;
    }

    public function getPicture()
    {
        if($this->filename)
            return Yii::$app->storage->getFile($this->filename);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(),['id' => 'user_id']);
    }

    public function like(User $user)
    {
        $redis = Yii::$app->redis;
        $k1 = "post:{$this->getId()}:likes";
        $k2 = "user:{$user->getId()}:likes";
        $k3 = "post:{$this->getId()}:unlikes";
        $k4 = "user:{$user->getId()}:unlikes";
        if($this->existsLike($user)) {
            $redis->srem($k1,$user->getId());
            $redis->srem($k2,$this->getId());
        }else if(!$this->existsLike($user) && !$this->existsUnlike($user)){
            $redis->sadd($k1, $user->getId());
            $redis->sadd($k2, $this->getId());
        }else if(!$this->existsLike($user) && $this->existsUnlike($user)) {
            $redis->sadd($k1, $user->getId());
            $redis->sadd($k2, $this->getId());
            $redis->srem($k3,$user->getId());
            $redis->srem($k4,$this->getId());
        }
    }

    protected function existsLike(User $user)
    {
        $redis = Yii::$app->redis;
        $k1 = "post:{$this->getId()}:likes";
        return $redis->sismember($k1,$user->getId());
    }

    protected function existsUnlike(User $user)
    {
        $redis = Yii::$app->redis;
        $k1 = "post:{$this->getId()}:unlikes";
        return $redis->sismember($k1,$user->getId());
    }
    public function unlike(User $user)
    {
        $redis = Yii::$app->redis;
        $k1 = "post:{$this->getId()}:unlikes";
        $k2 = "user:{$user->getId()}:unlikes";
        $k3 = "post:{$this->getId()}:likes";
        $k4 = "user:{$user->getId()}:likes";
        if($this->existsUnlike($user)) {
            $redis->srem($k1,$user->getId());
            $redis->srem($k2,$this->getId());
        }else if(!$this->existsUnlike($user) && !$this->existsLike($user)){
            $redis->sadd($k1, $user->getId());
            $redis->sadd($k2, $this->getId());
        }else if(!$this->existsUnlike($user) && $this->existsLike($user)){
            $redis->sadd($k1, $user->getId());
            $redis->sadd($k2, $this->getId());
            $redis->srem($k3,$user->getId());
            $redis->srem($k4,$this->getId());
        }

    }


    public function getCountLikes()
    {
        $redis = Yii::$app->redis;
        return $redis->scard("post:{$this->getId()}:likes");
    }

    public function getCountUnlikes()
    {
        $redis = Yii::$app->redis;
        return $redis->scard("post:{$this->getId()}:unlikes");
    }

    public function getId()
    {
        return $this->id;
    }


}
