<?php

namespace frontend\models;
use yii\db\ActiveRecord;
use Yii;
use app\models\Post;

class Feed extends ActiveRecord
{
    public function isReported(User $user)
    {
        $redis = Yii::$app->redis;
        $k = "post:{$this->post_id}:reports";
        return $redis->sismember($k,$user->getId());
    }

    public function countLikes()
    {
        $model = Post::findOne(['id' => $this->post_id]);
        return $model->getCountLikes();
    }
}