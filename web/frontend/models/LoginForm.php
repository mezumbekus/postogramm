<?php

namespace frontend\models;

use yii\base\Model;
use frontend\models\User;
use Yii;
use frontend\models\Action;
class LoginForm extends Model
{

    public $username;
    public $password;

    public function rules()
    {
        return [
            [['username','password'],'required'],
            ['password','validatePassword']
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'никнейм',
            'phonenumber' => 'Телефонный номер',
        ];
    }


    public function validatePassword($attribute)
    {
        $user =  User::findByUsername($this->username);
        if($user) {
           if(!$user->validatePassword($this->password)) {
               $this->addError($attribute,'Неправильное имя или пароль');
               return false;
           }
           $user->setActive();
           Yii::$app->user->login($user);

        }
        return true;
    }

}