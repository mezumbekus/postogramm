<?php

namespace frontend\models;


use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use Yii;

class User extends ActiveRecord implements IdentityInterface
{
    const NEW_STATUS = 0;
    const DELETED_STATUS = 1;
    const ACTIVE_STATUS = 2;
    const INACTIVE_STATUS = 4;


    public static function tableName()
    {
        return 'user';
    }

    public function rules()
    {
        return [
            [['username','firstname','lastname','email','password_hash','phonenumber','status','created_at','updated_at'],'required'],
            [['nickname','about','picture','type'],'safe'],
            [['email'],'unique','targetClass' => User::className()],
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findByUsername($name)
    {
        return static::findOne(['username' => $name]);
    }
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }


    public static function findIdentityByAccessToken($token, $type = null)
    {

    }

    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function getId()
    {
        return $this->id;
    }


    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function setActive()
    {
        /*$this->status = self::ACTIVE_STATUS; for mysql solutions
        return $this->save();*/
        $redis = Yii::$app->redis;
        $redis->sadd('activeusers',$this->getId());

    }
    public function setInactive()
    {
       /* $this->status = self::INACTIVE_STATUS; for mysql
        return $this->save();*/
        $redis = Yii::$app->redis;
        $redis->srem('activeusers',$this->getId());
    }
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function getNickname()
    {
        return $this->nickname ? $this->nickname : $this->getId();
    }

    public function getSubscribeKey()
    {
        return $this->getNickname().':'.$this->getId().':subscribes';
    }

    public function getFollowersKey()
    {
        return $this->getNickname().':'.$this->getId().':followers';
    }

    public function getFullName()
    {
        return $this->firstname.' '.$this->lastname;
    }
    public function followUser(User $user)
    {
        $redis = Yii::$app->redis;
        $redis->sadd($this->getSubscribeKey(),$user->getId());
        $redis->sadd($user->getFollowersKey(),$this->getId());
    }

    public function getSubscribes()
    {
        $redis = Yii::$app->redis;
        $subscribes = $redis->smembers($this->getSubscribeKey());
        return User::find()->select('id,firstname,lastname')->where(['id' => $subscribes])->orderBy('firstname')->asArray()->all();
    }

    public function getFollowers()
    {
        $redis = Yii::$app->redis;
        $followers = $redis->smembers($this->getFollowersKey());
        return User::find()->select('id,firstname,lastname')->where(['id' => $followers])->all();
    }

    public function unsubscribeUser(User $user)
    {
        $redis = Yii::$app->redis;
        $redis->srem($this->getSubscribeKey(),$user->getId());
        $redis->srem($user->getFollowersKey(),$this->getId());
    }

    public function countFollowers()
    {
        $redis = Yii::$app->redis;
        return $redis->scard($this->getFollowersKey());
    }

    public function countSubscribes()
    {
        $redis = Yii::$app->redis;
        return $redis->scard($this->getSubscribeKey());
    }

    public function getCommonFriends()
    {
        $redis = Yii::$app->redis;
        $user = Yii::$app->user->identity;
        $friends = $redis->sinter($user->getSubscribeKey(),$this->getFollowersKey());
        return User::find()->select('id,firstname,lastname')->where(['id' => $friends])->orderBy('firstname')->asArray()->all();
    }


    public function getPicture()
    {
        if($this->picture)
            return Yii::$app->storage->getFile($this->picture);
    }

    public function getOnlineCommonFriends()
    {
        $redis = Yii::$app->redis;
        $user = Yii::$app->user->identity;
        $friends = $redis->sinter($user->getSubscribeKey(),$this->getFollowersKey(),'activeusers');
        return User::find()->select('id,firstname,lastname')->where(['id' => $friends])->orderBy('firstname')->asArray()->all();
    }
    public function isSubscribed()
    {
        $redis = Yii::$app->redis;
        return $redis->sismember($this->getFollowersKey(),Yii::$app->user->identity->getId());
    }

    public function isSelfSubscribe()
    {
        return $this->getId() === Yii::$app->user->identity->getId();
    }
}