<?php

namespace frontend\models;

use yii\db\ActiveRecord;
use frontend\models\User;
use Yii;

class Comment extends ActiveRecord
{


    public function canEdit()
    {
        return $this->author_id === Yii::$app->user->identity->getId();
    }

    public static function tableName()
    {
        return 'comment';
    }
    public function getUser()
    {
        return $this->hasOne(User::className(),['id' => 'author_id']);
    }

    public function isOwner($userId)
    {
        return $this->find(['author_id' => $userId])->one();
    }
}