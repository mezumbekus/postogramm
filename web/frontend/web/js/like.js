$('.btn.like-btn').click(function(e){
    var params = {
        'id': $(this).attr('data-id')
    };
    $.post('/post/default/like',params,function(data) {
        if(data.success) {
            updateLikes(data.countLikes,data.countUnlikes);
        }
    });
    return false;
});

$('.btn.unlike-btn').click(function(e){
    var params = {
        'id': $(this).attr('data-id')
    };
    $.post('/post/default/unlike',params,function(data) {
        if(data.success) {
            updateLikes(data.countLikes,data.countUnlikes);
        }
    });
    return false;
});

function updateLikes(countLikes,countUnlikes)
{
    $('.btn.like-btn .count').text(countLikes);
    $('.btn.unlike-btn .count').text(countUnlikes);
}
jQuery(document).ready(function ($) {


    /*====== BACK TO TOP ======*/
    $('.back-to-top-page').each(function () {
        $('.back-to-top').on('click', function (event) {
            event.preventDefault();
            $('html, body').animate({scrollTop: 0}, 1500);
            return false;
        });
    });

});