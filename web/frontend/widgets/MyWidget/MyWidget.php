<?php

namespace frontend\widgets\MyWidget;

use yii\base\Widget;
use frontend\models\Employee;

class MyWidget extends Widget
{
    public $showLimit;
    public function run()
    {
        $items = Employee::getAllEmployees($this->showLimit);
        return $this->render('index',[
            'items' => $items,
        ]);
    }

}