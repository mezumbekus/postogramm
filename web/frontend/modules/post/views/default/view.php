<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<h1><?= $post->user->firstname.' '.$post->user->lastname?></h1>
<img src="<?= $post->getPicture()?>" alt="">
<p><?= $post->description ?></p>
Дата создания: <?= $post->created_at ?>
<div class="col-md-12">
    <a href="" class="btn btn-primary like-btn" data-id = "<?= $post->id ?>">Нраица <span class="count"><?= $post->getCountLikes() ?></span></a>
    <a href="" class="btn btn-primary unlike-btn" data-id = "<?= $post->id ?>">Не Нраица <span class="count"><?= $post->getCountUnlikes() ?></span></a>
</div>
    <div class="col-md-12">
        <?php
        $form = ActiveForm::begin();
        echo $form->field($comment,'comment');
        echo Html::submitButton('Разместить');
        ActiveForm::end();
        ?>
    </div>

<?php foreach ($comments as $comment): ?>
    <div class="col-md-12 comment">
        <span><?= $comment->user->firstname .' ' . $comment->user->lastname ?></span>
        <p><?= $comment->comment ?></p>
        <p><?= date('Y-m-d H:m:s',$comment->created_at) ?></p>
        <?php if($comment->canEdit()): ?>
            <a href="/post/edit/<?= $comment->id ?>">Редактировать</a>
            <a href="/post/delete/<?= $comment->id ?>">Удалить</a>
        <?php endif; ?>
    </div>

<?php endforeach; ?>

<?php

$this->registerJsFile('@web/js/like.js',[
        'depends' => \yii\web\JqueryAsset::className(),
]);

?>