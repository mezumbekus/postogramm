<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="post-default-index">
    <h1>Создать пост</h1>
    <?php $form = ActiveForm::begin();
        echo $form->field($model,'picture')->fileInput();
        echo $form->field($model,'description');
        echo Html::submitButton('Создать');
        ActiveForm::end();
    ?>
</div>
