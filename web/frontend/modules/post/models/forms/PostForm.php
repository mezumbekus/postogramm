<?php

namespace frontend\modules\post\models\forms;
use app\models\Post;
use yii\base\Model;
use Yii;
use frontend\models\User;
use noam148\imagemanager\models\ImageManager;

class PostForm extends Model
{
    const MAX_DESC_LIMIT = 100;
    public $picture;
    public $description;
    private $user;

    public function rules()
    {
        return [
            [['picture'],'file',
                'skipOnEmpty' => false,
                'extensions' => ['jpg','png'],
                'checkExtensionByMimeType' => true,
                'maxSize' => $this->getMaxFileSize()
            ],
            [['description'],'string','max' => self::MAX_DESC_LIMIT],
        ];
    }

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function save()
    {
        if($this->validate()) {
            $post = new Post();
            $post->created_at = time();
            $post->user_id = $this->user->getId();
            $post->filename = Yii::$app->storage->saveUploadFile($this->picture);
            $post->description = $this->description;
            if($post->save(false)) {
                Yii::$app->feedService->pushFeed($this->user,$post);
            }
        }

    }

    private function getMaxFileSize()
    {
        return Yii::$app->params['maxFileSize'];
    }
}
