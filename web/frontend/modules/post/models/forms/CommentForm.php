<?php

namespace frontend\modules\post\models\forms;

use yii\base\Model;
use frontend\models\Comment;

class CommentForm extends Model
{
    public $comment;
    public $author_id;
    public $created_at;
    public $post_id;

    public function rules()
    {
        return [
          [['comment','author_id','created_at','post_id'],'required'],
        ];
    }

    public function save()
    {
        $model = new Comment();
        $model->author_id = $this->author_id;
        $model->comment = $this->comment;
        $model->created_at = $this->created_at;
        $model->post_id = $this->post_id;
        return $model->save();

    }

}