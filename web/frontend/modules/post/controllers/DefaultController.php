<?php

namespace frontend\modules\post\controllers;


use frontend\modules\post\models\forms\CommentForm;
use yii\web\Controller;
use app\models\Post;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use frontend\modules\post\models\forms\PostForm;
use frontend\models\Comment;

use Yii;

/**
 * Default controller for the `post` module
 */
class DefaultController extends Controller
{

    public function actionCreate()
    {
        $model = new PostForm(Yii::$app->user->identity);
        if($model->load(Yii::$app->request->post())) {
            $model->picture = UploadedFile::getInstance($model,'picture');
            if($model->save()) {
                Yii::$app->session->setFlash('success','Пост создан');
                return $this->goHome();
            }
        }
        return $this->render('create',[
            'model' => $model,
        ]);
    }

    public function actionReport()
    {
        if(Yii::$app->user->isGuest)
            return $this->redirect(['/user/profile/login']);
        $id = Yii::$app->request->post('id');
        $model = Post::findOne(['id' => $id]);
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->sendReport(Yii::$app->user->identity)){
            $model->reports++;
            $model->save();
            return [
                'success' => true,
                'data' => 'Post reported',
            ];
        }

        return [
            'success' => false,
            'data' => 'Error!',
        ];
    }


    public function actionViewAll()
    {
        $posts = Post::find()->asArray()->all();
        return [
          'posts' => $posts,
        ];
    }
    public function actionView($id)
    {
        $comment = new CommentForm();
        if($comment->load(Yii::$app->request->post())){
            $comment->author_id = Yii::$app->user->identity->getId();
            $comment->created_at = time();
            $comment->post_id = $id;
            $comment->save();
            $this->redirect(['/post/'.$id]);
        }
        $comments = Comment::find()->where(['post_id' => $id])->all();
        return $this->render('view',[
            'post' => $this->findPost($id),
            'comment' => $comment,
            'comments' => $comments,
        ]);
    }

    public function actionLike()
    {
        if(Yii::$app->user->isGuest)
            $this->redirect(['/user/default/login']);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->request->post('id');
        $post = $this->findPost($id);
        $currentUser = Yii::$app->user->identity;
        $post->like($currentUser);
        return [
            'success' => true,
            'countLikes' => $post->getCountLikes(),
            'countUnlikes' => $post->getCountUnlikes(),
        ];
    }

    public function actionUnlike()
    {
        if(Yii::$app->user->isGuest)
            $this->redirect(['/user/default/login']);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->request->post('id');
        $post = $this->findPost($id);
        $currentUser = Yii::$app->user->identity;
        $post->unlike($currentUser);
        return [
            'success' => true,
            'countLikes' => $post->getCountLikes(),
            'countUnlikes' => $post->getCountUnlikes(),
        ];

    }

    private function findPost($id)
    {
        if($post = Post::findOne($id)){
            return $post;
        }
        throw new NotFoundHttpException('Нет такого поста!');
    }
}
