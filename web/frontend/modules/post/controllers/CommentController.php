<?php

namespace frontend\modules\post\controllers;

use yii\web\Controller;
use frontend\modules\post\models\forms\CommentForm;
use frontend\models\Comment;
use Yii;

class CommentController extends Controller
{
    public function actionEdit($id)
    {
        $model = Comment::findOne(['id' => $id]);
        if($model->load(Yii::$app->request->post())) {
            $model->save();
            $this->goBack();
        }
        return $this->render('edit',[
           'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {

    }
}