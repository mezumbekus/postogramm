<?php

namespace frontend\modules\user\controllers;

use yii\web\Controller;
use frontend\models\User;
use yii\web\NotFoundHttpException;
use Faker\Factory;
use Yii;
use frontend\modules\user\models\forms\PictureForm;
use yii\web\Response;
use yii\web\UploadedFile;

class ProfileController extends Controller
{
    public function actionView($nickname)
    {


        return $this->render('view', [
            'user' => $this->findUser($nickname),
            'pictureModel' => $pictureModel,
        ]);
    }

    public function actionUploadPicture()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new PictureForm();
        $model->picture = UploadedFile::getInstance($model, 'picture');
        if ($model->validate()) {
            $user = Yii::$app->user->identity;
            $user->picture = Yii::$app->storage->saveUploadFile($model->picture);
            if($user->save(false,['picture'])) {
                return [
                    'success' => true,
                    'pictureUri' => Yii::$app->storage->getFile($user->picture),
                ];
            }

        }
        return [
            'success' => false,
            'errors' => $model->getErrors(),
        ];
    }



    private function findUser($nickname)
    {
        $user = User::find()->where(['id' => $nickname])->orWhere(['nickname' => $nickname])->one();
        if($user)
            return $user;
        throw new NotFoundHttpException('No user');
    }

    public function actionGenerate()
    {
        $faker = Factory::create();
        for($i = 0; $i < 10; $i++){
            $created = time();
            $user = new User([
                'username' => $faker->name(),
                'email' => $faker->email(),
                'about' => $faker->text(),
                'nickname' => preg_replace('~[\s|.]+~','',$faker->name()),
                'password_hash' => Yii::$app->security->generateRandomString(12),
                'created_at' => $created,
                'updated_at' => $created,
                'firstname' => $faker->name,
                'lastname' => $faker->name,
                'phonenumber' => $faker->phoneNumber(),
                'status' => User::NEW_STATUS,
            ]);
            $user->save();
            Yii::$app->response->redirect(['site/index']);
        }
    }

    public function actionSubscribe($id)
    {
        if(Yii::$app->user->isGuest)
            return Yii::$app->response->redirect(['user/default/login']);
        $currentUser = Yii::$app->user->identity;
        $user = User::findIdentity($id);
        $currentUser->followUser($user);
        return $this->redirect(['/user/profile/view','nickname' => $user->getId()]);
    }

    public function actionUnsubscribe($id)
    {
        if(Yii::$app->user->isGuest)
            return Yii::$app->response->redirect(['user/default/login']);
        $currentUser = Yii::$app->user->identity;
        $user = User::findIdentity($id);
        $currentUser->unsubscribeUser($user);
        return $this->redirect(['/user/profile/view','nickname' => $user->getId()]);
    }

    public function actionEdit()
    {
        if(Yii::$app->user->isGuest)
            return Yii::$app->response->redirect(['user/default/login']);
        $pictureModel = new PictureForm();
        $user = Yii::$app->user->identity;
        if($user->load(Yii::$app->request->post())) {
            Yii::$app->session->setFlash('success','Сохранено');
            $user->save();

        }
        return $this->render('edit',[
            'user' => $user,
            'pictureModel' => $pictureModel,
        ]);
    }
}
