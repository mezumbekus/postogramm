<?php

namespace frontend\modules\user\controllers;

use frontend\models\LoginForm;
use frontend\models\SignupForm;
use frontend\models\User;
use Yii;
use yii\web\Controller;
use frontend\modules\models\RecoveryPassword;


/**
 * Default controller for the `user` module
 */
class DefaultController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSignup()
    {
        if(!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();
        if($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $user = new User();
            $user->firstname = $model->firstname;
            $user->username = $model->username;
            $user->lastname = $model->lastname;
            $user->email = $model->email;
            $user->phonenumber = $model->phonenumber;
            $user->created_at = $user->updated_at = time();
            $user->status = User::NEW_STATUS;
            $user->password_hash = Yii::$app->security->generatePasswordHash($model->password);
            if($user->save()) {
                Yii::$app->user->login($user);
                return $this->goHome();
            }
        }
        return $this->render('signup',[
            'model' => $model,
        ]);

    }

    public function actionLogin()
    {
        if(!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if($model->load(Yii::$app->request->post())) {
            if ($model->validatePassword('username')) {
                return $this->goHome();
            }

        }
        return $this->render('login',
            ['model' => $model]
        );
    }

    public function actionRecovery()
    {
        $hash = Yii::$app->security->generateRandomString(55);
        $model = new RecoveryPassword();
        $model->hash = $hash;
        $model->expired = strtotime('+1 day');
     /*   $link =
        if($model->save()) {
            Yii::$app->mailer
        }*/
    }

    public function actionLogout()
    {
        Yii::$app->user->identity->setInactive();
        Yii::$app->user->logout();
        return $this->redirect(['/site/index']);
    }

}
