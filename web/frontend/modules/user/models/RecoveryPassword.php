<?php

namespace frontend\modules\models;

use yii\db\ActiveRecord;

class RecoveryPassword extends ActiveRecord
{
    public function rules()
    {
        return [
            [['hash','expired'],'required'],
        ];
    }

}
