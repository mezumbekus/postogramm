<?php
/**
 * @var model SignForm
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();
echo $form->field($model,'username');
echo $form->field($model,'password');
echo $form->field($model,'firstname');
echo $form->field($model,'lastname');
echo $form->field($model,'email');
echo $form->field($model,'phonenumber');
?>
<div class="form-group">
    <div>
        <?= Html::submitButton('Регистрация',['class' => 'btn btn-success'])?>
    </div>
</div>
<?php ActiveForm::end(); ?>
