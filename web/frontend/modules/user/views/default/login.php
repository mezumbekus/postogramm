<?php
/**
 * @var model SignForm
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();
echo $form->field($model,'username');
echo $form->field($model,'password');
?>
    <div class="form-group">
        <div>
            <?= Html::submitButton('Войти',['class' => 'btn btn-success'])?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<p><?= Html::a('забыли пароль?',['/user/default/recovery']) ?></p>
