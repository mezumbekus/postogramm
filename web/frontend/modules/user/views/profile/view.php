<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;

?>

<h1>Hello <?= Html::encode($user['firstname'])?></h1>
О себе:
<div><img src="<?= $user->getPicture() ?>" alt=""></div>
<p><?= HtmlPurifier::process($user['about'])?></p>
<?php if(!$user->isSubscribed()): ?>
    <?php if(!$user->isSelfSubscribe()): ?>
        <a href="<?= Url::to(['/user/profile/subscribe/','id'=> $user['id']])?>" class="col-2 mr-2 btn-info btn">Подписаться</a>
    <?php endif; ?>
<?php else: ?>
<a href="<?= Url::to(['/user/profile/unsubscribe/','id'=> $user['id']])?>" class="col-2 mr-2 btn-info btn">Отписаться</a>
<?php endif; ?>
<div class="col-2">Подписан: <?= $user->countSubscribes() ?></div>
<div class="col-2">Подписаны: <?= $user->countFollowers() ?></div>
<pre>
<?php print_r($user->getSubscribes()) ?>
</pre>
<pre>
<?php print_r($user->getFollowers()) ?>
</pre>
