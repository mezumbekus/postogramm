<?php

use dosamigos\fileupload\FileUpload;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$form = ActiveForm::begin();
echo $form->field($user,'username');
echo $form->field($user,'firstname');
echo $form->field($user,'lastname');
echo $form->field($user,'email');
echo $form->field($user,'phonenumber');
echo Html::submitButton('Сохранить');
ActiveForm::end();
echo FileUpload::widget([
    'model' => $pictureModel,
    'attribute' => 'picture',
    'url' => ['/user/profile/upload-picture'], // your url, this is just for demo purposes,
    'options' => ['accept' => 'image/*'],
    'clientOptions' => [
        'maxFileSize' => 2000000
    ],
    // Also, you can specify jQuery-File-Upload events
    // see: https://github.com/blueimp/jQuery-File-Upload/wiki/Options#processing-callback-options
    'clientEvents' => [
        'fileuploaddone' => 'function(e, data) {
                                console.log(data.result.pictureUri);
                                $(".user-pic").attr("src",data.result.pictureUri);
                            }',
        'fileuploadfail' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
    ],
]);
?>
<img src="<?= $user->getPicture()?>" alt="" class="user-pic">
