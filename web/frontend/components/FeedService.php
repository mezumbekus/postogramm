<?php

namespace frontend\components;
use yii\base\Component;
use app\models\Post;
use frontend\models\Feed;

class FeedService extends Component
{
    public function pushFeed($user,$post)
    {
        $followers = $user->getFollowers();

        foreach ($followers as $follower) {
            $feed = new Feed();
            $feed->user_id = $follower->getId();
            $feed->author_id = $user->getId();
            $feed->author_name = $user->getFullName();
            $feed->author_nickname = $user->getNickname();
            $feed->author_picture = $user->getPicture();
            $feed->post_id = $post->getId();
            $feed->post_filename = $post->filename;
            $feed->post_description = $post->description;
            $feed->post_created_at = $post->created_at;
            $feed->save();
        }
    }

}