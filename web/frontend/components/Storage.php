<?php

namespace frontend\components;

use yii\base\Component;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use Yii;
class Storage extends Component implements StorageInterface
{
    private $filename;

    public function saveUploadFile(UploadedFile $file)
    {
        $path = $this->preparePath($file);

        if($file && $file->saveAs($path)) {
            return $this->filename;
        }
    }

    public function getFile(string $filename)
    {
        return Yii::$app->params['storageUri'].$filename;
    }
    protected function getStoragePath()
    {
        return Yii::getAlias(Yii::$app->params['storagePath']);
    }
    protected function preparePath(UploadedFile $file)
    {
        $filename = $this->getFileName($file);
        $this->filename = $filename;
        $path = $this->getStoragePath().$filename;
        $path = FileHelper::normalizePath($path);
        if(FileHelper::createDirectory(dirname($path))) {
            return $path;
        }
    }

    protected function getFileName(UploadedFile $file)
    {
        $hash = sha1_file($file->tempName);
        $name = substr_replace($hash,'/',2,0);
        $name = substr_replace($name,'/',5,0);
        return $name . '.' . $file->extension;
    }
}