<?php
namespace frontend\controllers;

use frontend\models\Feed;
use yii\web\Controller;
use Yii;

 class SiteController extends Controller
 {
     public function actionIndex()
     {
         if(Yii::$app->user->isGuest)
             $this->redirect(['/user/default/login']);
         $currentUser = Yii::$app->user->getId();
         $feedUsers = Feed::find()->where(['user_id' => $currentUser])->all();
         return $this->render('index', [
                'feedUsers' => $feedUsers,
                'user' => Yii::$app->user->identity,
             ]);
     }
 }