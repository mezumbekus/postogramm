<?php
return [
    'adminEmail' => 'admin@example.com',
    'maxStringLength' => 20,
    'maxFileSize' => 1024*1024*2,
    'storagePath' => '@frontend/web/uploads/',
    'storageUri' => '/uploads/',
    'postPicture' => [
        'maxWidth' => 1024,
        'maxHeight' => 768,
    ],
];
