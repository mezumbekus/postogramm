<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use backend\models\User;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
        $admin = $auth->createRole('admin');
        $manager = $auth->createRole('manager');
        $auth->add($admin);
        $auth->add($manager);
        $viewUsers = $auth->createPermission('viewUsers');
        $deleteUsers = $auth->createPermission('deleteUsers');
        $viewPosts = $auth->createPermission('viewPosts');
        $deletePosts = $auth->createPermission('deletePosts');
        $approvePosts = $auth->createPermission('approvePosts');
        $auth->add($viewUsers);
        $auth->add($deleteUsers);
        $auth->add($viewPosts);
        $auth->add($deletePosts);
        $auth->add($approvePosts);
        $auth->addChild($admin,$viewUsers);
        $auth->addChild($manager,$viewPosts);
        $auth->addChild($admin,$manager);
        $auth->addChild($admin,$deletePosts);
        $auth->addChild($admin,$deleteUsers);
        $admin_user = User::findByUsername('spock');
        $manager_user = User::findByUsername('spock2');
        $auth->assign($admin,$admin_user->getId());
        $auth->assign($manager,$manager_user->getId());
    }

}