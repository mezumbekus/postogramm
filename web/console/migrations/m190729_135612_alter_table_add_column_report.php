<?php

use yii\db\Migration;

/**
 * Class m190729_135612_alter_table_add_column_report
 */
class m190729_135612_alter_table_add_column_report extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('post','reports','string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('post','reports');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190729_135612_alter_table_add_column_report cannot be reverted.\n";

        return false;
    }
    */
}
