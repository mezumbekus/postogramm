<?php

use yii\db\Migration;
use backend\models\User;
/**
 * Class m190730_034252_fill_roles
 */
class m190730_034252_fill_roles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->createRole('admin');
        $manager = $auth->createRole('manager');
        $viewUsers = $auth->createPermission('viewUsers');
        $deleteUsers = $auth->createPermission('deleteUsers');
        $viewPosts = $auth->createPermission('viewPosts');
        $deletePosts = $auth->createPermission('deletePosts');
        $approvePosts = $auth->createPermission('approvePosts');
        $auth->add($viewUsers);
        $auth->add($deleteUsers);
        $auth->add($viewPosts);
        $auth->add($deletePosts);
        $auth->add($approvePosts);
        $auth->addChild($manager,$viewUsers);
        $auth->addChild($manager,$viewPosts);
        $auth->addChild($admin,$manager);
        $auth->addChild($admin,$deletePosts);
        $auth->addChild($admin,$deleteUsers);
        $admin_user = User::findByUsername('spock');
        $manager_user = User::findByUsername('spock2');
        $auth->assign($admin,$admin_user->getId());
        $auth->assign($manager,$manager_user->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190730_034252_fill_roles cannot be reverted.\n";

        return false;
    }


}
