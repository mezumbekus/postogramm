<?php

use yii\db\Migration;

/**
 * Class m190728_113328_mod_comment_table
 */
class m190728_113328_mod_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('comment','post_id','string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190728_113328_mod_comment_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190728_113328_mod_comment_table cannot be reverted.\n";

        return false;
    }
    */
}
