<?php

use yii\db\Migration;

/**
 * Class m190724_121621_recovery_password_table
 */
class m190724_121621_recovery_password_table extends Migration
{

    public function safeUp()
    {
        $this->createTable('password_recovery_requests',[
            'id' => $this->primaryKey(),
            'hash' => $this->string()->notNull(),
            'expired' => $this->string()->notNull(),
        ]);

    }

    public function safeDown()
    {
        $this->dropTable('password_recovery_requests');
    }

}
