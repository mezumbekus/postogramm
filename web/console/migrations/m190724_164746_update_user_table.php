<?php

use yii\db\Migration;

/**
 * Class m190724_164746_update_user_table
 */
class m190724_164746_update_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user','about','string');
        $this->addColumn('user','type','string');
        $this->addColumn('user','nickname','string');
        $this->addColumn('user','picture','string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user','about');
        $this->dropColumn('user','type');
        $this->dropColumn('user','nickname');
        $this->dropColumn('user','picture');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190724_164746_update_user_table cannot be reverted.\n";

        return false;
    }
    */
}
