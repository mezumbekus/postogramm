<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

             [
                     'attribute' => 'id',
                    'format' => 'raw',
                    'value' => function($post) {
                        return Html::a($post->id,['view','id' => $post->id]);
                    }
             ],
            'user_id',
            'filename',
            'description:ntext',
            'created_at:datetime',
            'reports',

            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {approve} {delete}',
                    'buttons' => [
                         'approve' => function ($url,$post) {
                            return Html::a('Подтвердить',['approve','id' => $post->id]);
                        }
                    ]
            ],
        ],
    ]); ?>


</div>
