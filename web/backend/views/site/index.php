<?php

/* @var $this yii\web\View */

$this->title = 'Admin';
use yii\helpers\Url;

?>
<div class="site-index">


    <div class="body-content">

        <div class="row">
            <div class="col-lg-4 col-lg-offset-2">
                <h2>Manage</h2>


                <p><a class="btn btn-default" href="<?= Url::to(['/reports/manage'])?>"><?= Yii::t('index','reports') ?> &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Manage</h2>


                <p><a class="btn btn-default" href="<?= Url::to(['/user/manage'])?>"><?= Yii::t('index','users') ?> &raquo;</a></p>
            </div>

        </div>

    </div>
</div>
